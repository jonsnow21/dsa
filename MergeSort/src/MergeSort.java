public class MergeSort {

    private void merge(int[] arr, int l, int m, int r) {
        int size1 = m - l + 1;
        int size2 = r - m;

        int L[] = new int[size1];
        int R[] = new int[size2];

        for ( int i = 0; i < size1; i++ )
            L[i] = arr[l + i];
        for ( int j = 0; j < size2; j++ )
            R[j] = arr[m + 1 + j];

        int i = 0, j = 0, k = l;
        while ( i < size1 && j < size2 ) {
            if ( L[i] <= R[j] ) {
                arr[k] = L[i];
                i++;
            } else {
                arr[k] = R[j];
                j++;
            }
            k++;
        }

        while ( i < size1 ) {
            arr[k] = L[i];
            i++;
            k++;
        }

        while ( j < size2 ) {
            arr[k] = R[j];
            j++;
            k++;
        }
    }

    private void sort(int[] arr, int l, int r) {
        if( l < r ) {
            int m = (r + l) / 2;
            sort(arr, l, m);
            sort(arr, m + 1, r);
            merge(arr, l, m, r);
        }
    }

    public static void main(String[] args) {
        int[] array = {1, 8, 7, 2, 5, 4};
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }

        MergeSort ob = new MergeSort();
        ob.sort( array, 0, array.length - 1 );
        System.out.println("After merge sort");

        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
    }
}
