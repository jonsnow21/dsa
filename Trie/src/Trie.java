/**
 * Created by neeraj on 14/2/17.
 */
public class Trie {
    private TrieNode root;

    public Trie() {
        root = new TrieNode();
    }

    public void insert(String word) {
        TrieNode p = root;
        for (int i = 0; i < word.length(); i++) {
            char c = word.charAt(i);
            int index = c - 'a';
            if (p.getArr()[index] == null) {
                TrieNode temp = new TrieNode();
                p.getArr()[index] = temp;
                p = temp;
            } else {
                p = p.getArr()[index];
            }
        }

        p.setLeaf(true);
    }

    public boolean search(String word) {
        TrieNode p = searchNode(word);
        if (p == null) return false;
        else if (p.isLeaf()) return true;
        return false;
    }

    public boolean startsWith(String prefix) {
        TrieNode p = searchNode(prefix);
        return p != null;
    }

    public void delete(String word) {
        TrieNode p = searchNode(word);
        if (p == null) System.out.println("Word not found");
        else if (!p.isLeaf()) System.out.println("Word is a prefix");
        else {
            deleteWord(root.getArr()[word.charAt(0) - 'a'], word, 0);
        }
    }

    private void deleteWord(TrieNode p, String word, int n) {
        if (n == word.length() - 1) {
            p.getArr()[word.charAt(n) - 'a'] = null;
            return;
        }

        deleteWord(p.getArr()[word.charAt(n + 1) - 'a'], word, n + 1);
        p.getArr()[word.charAt(n) - 'a'] = null;
    }

    private TrieNode searchNode(String word) {
        TrieNode p = root;
        for (int i = 0; i < word.length(); i++) {
            char c = word.charAt(i);
            int index = c - 'a';
            if (p.getArr()[index] == null) {
                return null;
            } else {
                p = p.getArr()[index];
            }
        }

        if (p == root) return null;
        return p;
    }
}
