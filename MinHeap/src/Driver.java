/**
 * Created by neeraj on 17/2/17.
 */
public class Driver {
    public static void main(String[] args) {
        MinHeap heap = new MinHeap(10);
        heap.insert(4);
        heap.insert(6);
        heap.insert(1);
        heap.insert(11);
        heap.insert(7);
        heap.insert(3);
        heap.insert(2);
        heap.insert(12);
        heap.insert(24);
        heap.insert(8);
        heap.insert(16);
        heap.insert(10);

        /*System.out.print(heap.extractMin() + " ");
        System.out.print(heap.extractMin() + " ");
        System.out.print(heap.extractMin() + " ");*/

        heap.heapSort();
    }
}
