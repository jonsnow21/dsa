/**
 * Created by neeraj on 14/2/17.
 */
public class TrieNode {
    private TrieNode[] arr;
    private boolean isLeaf;

    public TrieNode() {
        this.arr = new TrieNode[26];
    }

    public TrieNode[] getArr() {
        return arr;
    }

    public void setArr(TrieNode[] arr) {
        this.arr = arr;
    }

    public boolean isLeaf() {
        return isLeaf;
    }

    public void setLeaf(boolean leaf) {
        isLeaf = leaf;
    }
}
