import java.util.Scanner;

/**
 * Created by neeraj on 14/2/17.
 */
public class Driver {
    public static void main(String[] args) {
        Trie trie = new Trie();
        String[] a = {"the", "a", "there", "answer", "any",
                "by", "bye", "their", "z"};
        for (String word: a) {
            trie.insert(word);
        }

        trie.delete("z");

        while (true) {
            Scanner s = new Scanner(System.in);
            String find = s.next();
            if (trie.search(find)) {
                System.out.println("yes");
            } else {
                break;
            }
        }
    }
}
