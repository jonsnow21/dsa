import java.util.Arrays;

/**
 * Created by neeraj on 17/2/17.
 */
public class MinHeap {
    private int[] heapArray;
    private int sizeOfArray;
    private int sizeOfHeap;

    MinHeap(int sizeOfArray) {
        this.sizeOfArray = sizeOfArray;
        heapArray = new int[sizeOfArray];
        sizeOfHeap = 0;
    }

    public int parent(int i) {
        return (i - 1) / 2;
    }

    public int left(int i) {
        return i * 2 + 1;
    }

    public int right(int i) {
        return i * 2 + 2;
    }

    public int getMin() {
        return heapArray[0];
    }

    public void insert(int k) {
        if (sizeOfArray == sizeOfHeap) {
            sizeOfArray *= 2;
            heapArray = Arrays.copyOf(heapArray, sizeOfArray);
        }

        sizeOfHeap++;
        int i = sizeOfHeap - 1;
        heapArray[i] = k;

        while (i != 0 && heapArray[parent(i)] > heapArray[i]) {
            swap(i, parent(i));
            i = parent(i);
        }
    }

    private void swap(int i, int j) {
        int temp = heapArray[i];
        heapArray[i] = heapArray[j];
        heapArray[j] = temp;
    }

    public int extractMin() {
        if (sizeOfHeap <= 0) {
            return Integer.MAX_VALUE;
        }

        if (sizeOfHeap == 1) {
            sizeOfHeap--;
            return heapArray[0];
        }

        int root = heapArray[0];
        heapArray[0] = heapArray[sizeOfHeap - 1];
        sizeOfHeap--;
        minHeapify(0);

        return root;
    }

    private void minHeapify(int i) {
        int l = left(i);
        int r = right(i);
        int smallest = i;

        if (l < sizeOfHeap && heapArray[i] > heapArray[l]) {
            smallest = l;
        }

        if (r < sizeOfHeap && heapArray[r] < heapArray[smallest]) {
            smallest = r;
        }

        if (smallest != i) {
            swap(i, smallest);
            minHeapify(smallest);
        }
    }

    public void decreaseKey(int i, int key) {
        heapArray[i] = key;

        while (i != 0 && heapArray[parent(i)] > heapArray[i]) {
            swap(i, parent(i));
            i = parent(i);
        }
    }

    public void deleteKey(int i) {
        decreaseKey(i, Integer.MIN_VALUE);
        extractMin();
    }

    public void heapSort() {
        int k = sizeOfHeap;
        for (int i = 0; i < k; i++) {
            System.out.print(extractMin() + " ");
        }
    }

}
